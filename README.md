# Toilet paper continuous delivery

Query stock of toilet paper at nearby DM drug stores and send a message via Telegram API.

## Project Setup

You need some CI/CD Variables

|   |   |
|---|---|
| TELEGRAM_BOT_TOKEN | BotFather token  |
| TELEGRAM_CHATID | curl https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/getUpdates | jq .message.chat.id  |
| COORDINATES | Latitude and longitude of your Location like `49.72347100000893%2C10.010620500007974` |

