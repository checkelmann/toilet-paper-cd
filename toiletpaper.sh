#!/bin/bash

curl -s https://store-data-service.services.dmtech.com/stores/nearby/$COORDINATES/1 > stores.json
arr=( $(cat stores.json | jq -r '.stores[].storeNumber') )

for i in "${arr[@]}"
do	    
    STOCK=$(curl -s "https://products.dm.de/store-availability/DE/availability?dans=595420,708997,137425,28171,485698,799358,863567,452740,610544,846857,709006,452753,879536,452744,485695,853483,594080,504606,593761,525943,842480,535981,127048,524535&storeNumbers=$i" | jq '[.storeAvailabilities[][0].stockLevel] | add')
    STORE=$(cat stores.json | jq -c -r '.stores[] | select(.storeNumber == "'$i'") | .address.street, .address.city')
    echo "$STORE: $STOCK\n" >> result.txt
done

MESSAGE=$(cat result.txt)

json_payload="
{\"chat_id\": \"${TELEGRAM_CHATID}\", \"text\": \"$MESSAGE\", \"disable_notification\": true}
"

echo $json_payload > payload.json

curl -X POST \
            -H 'Content-Type: application/json' \
            -d @payload.json \
        https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage